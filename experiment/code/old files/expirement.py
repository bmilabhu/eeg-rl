import pygame
import numpy as np
import random
import socket
import struct
##from psychopy import core
##from psychopy import parallel

BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
BLUE =  (  0,   0, 255)
GREEN = (  0, 255,   0)
RED =   (255,   0,   0)

HORIZ_COUNT = 6
VERTIC_COUNT = 6
LEARNING_RATE = 1000
SCREEN_SIZE = np.array([800, 600])
GRID_COUNT = np.array([HORIZ_COUNT,VERTIC_COUNT])
RECT_SIZE = SCREEN_SIZE/GRID_COUNT

MOVEEVENT = pygame.USEREVENT+1
MOVEEVENT_T = 1000;

KEY_UP = "up"
KEY_DOWN = "down"
KEY_RIGHT = "right"
KEY_LEFT = "left"

ACTIONS = {KEY_UP:np.array([0, -1]),
            KEY_DOWN:np.array([0, 1]),
           KEY_RIGHT:np.array([1, 0]),
            KEY_LEFT:np.array([-1, 0]),}


class Udp():

    def __init__(self):
        UDP_IP="127.0.0.1"
        UDP_PORT=27643
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setblocking(0)
        self.sock.bind((UDP_IP,UDP_PORT))

    def update(self):
        try:
            data, addr = self.sock.recvfrom(4)
            # data=data.decode('ascii')
            data = struct.unpack(">f", data)[0]
            print(data)
            # data = int("{:02}".format(ord(data)))
            ev = pygame.event.Event(pygame.USEREVENT, {'data': data, 'addr': addr})
            #print(data)
            pygame.event.post(ev)
        except socket.error as e:
            pass


class cell:
    def __init__(self,x,y):
        self.position = np.array([x,y])
        self.actions_probs = {}
        self.likelihood = 0
        if(x==0 and y == 0):
            self.likelihood = 1

        for actionName in ACTIONS:
            self.actions_probs[actionName] = 1
        if(x == HORIZ_COUNT-1):
            self.actions_probs.pop(KEY_RIGHT,None)
        if(x == 0):
            self.actions_probs.pop(KEY_LEFT,None)
        if(y == 0):
            self.actions_probs.pop(KEY_UP,None)
        if(y == VERTIC_COUNT-1):
            self.actions_probs.pop(KEY_DOWN,None)
        self.normalizeProbs()

    def updateProb(self,action,p):
        self.actions_probs[action] = p
        self.normalizeProbs()

    def normalizeProbs(self):
        sum = 0
        for actionName in self.actions_probs:
            sum = sum + self.actions_probs[actionName]
        for actionName in self.actions_probs:
            self.actions_probs[actionName] = self.actions_probs[actionName]/float(sum)


class agent:
    def __init__(self):
        #self.position = np.array([HORIZ_COUNT/2,VERTIC_COUNT/2]).astype(np.int)
        self.position = np.array([0,0]).astype(np.int)
    def draw_agent(self):
        x = RECT_SIZE[0]*self.position[0]
        y = RECT_SIZE[1]*self.position[1]
        rect = [x,y,RECT_SIZE[0],RECT_SIZE[1]]
        pygame.draw.rect(screen,BLUE,rect,0)

    def weighted_choice(self,choices):
        total = sum(w for c,w in choices.items())
        r = random.uniform(0, total)
        upto = 0
        for c,w in choices.items():
            if upto + w >= r:
                return c
            upto += w
        assert False, "Shouldn't get here"

    def next_step(self):
        cell = GridMap[self.position[0]][self.position[1]]
        step = self.weighted_choice(cell.actions_probs)
        last_position = self.position
        self.position = self.position+ACTIONS[step]
        return step, last_position

def check_correctness():
    key = pygame.key.get_pressed()
    if(key == 'r'):
        return 1
    elif(key == "w"):
        return -1
    return 0


def updateLikelihood():
    l1=l2=l3=l4=0
    for i in range(HORIZ_COUNT):
        for j in range(VERTIC_COUNT):
            try:
                l1 = GridMap[i-1][j].likelihood * GridMap[i-1][j].actions_probs[KEY_RIGHT]
            except:
                pass
            try:
                l2 = GridMap[i+1][j].likelihood * GridMap[i+1][j].actions_probs[KEY_LEFT]
            except:
                pass
            try:
                l3 = GridMap[i][j-1].likelihood * GridMap[i][j-1].actions_probs[KEY_DOWN]
            except:
                pass
            try:
                l4 =  GridMap[i][j + 1].likelihood * GridMap[i][j + 1].actions_probs[KEY_UP]
            except:
                pass
            GridMap[i][j].likelihood = max(l1,l2,l3,l4)#,GridMap[i][j].likelihood)
            if(i==0 and j == 0):
                GridMap[i][j].likelihood=1


def colorGrid():
    for i in range(HORIZ_COUNT):
        for j in range(VERTIC_COUNT):
            x = RECT_SIZE[0] * i
            y = RECT_SIZE[1] * j
            cell = GridMap[i][j]
            rect = [x, y, RECT_SIZE[0], RECT_SIZE[1]]

            pygame.draw.rect(screen, (int(min(255,cell.likelihood*255)),255,255), rect, 0)

def updatePropabilities(position, action,correct):
    if(action != None and correct != 0):
        if correct==1:
            print("correct!")
            m = LEARNING_RATE
        elif correct == -1:
            print("incorrect!")
            m = 1/LEARNING_RATE
        x = (GridMap[position[0]][position[1]].actions_probs[action]*m)
        #x += max(GridMap[position[0]+ACTIONS[action][0]][position[1]+ACTIONS[action][1]].actions_probs.values())
        p = x*m/(1+(m-1)*x)
        GridMap[position[0]][position[1]].updateProb(action,p)

def drawTarget(position):
    x = RECT_SIZE[0] * position[0]
    y = RECT_SIZE[1] * position[1]
    rect = [x, y, RECT_SIZE[0], RECT_SIZE[1]]
    pygame.draw.rect(screen, RED, rect, 0)

def drawGrid():
    for i in range(GRID_COUNT[0]):
        start_pos = (SCREEN_SIZE[0]/GRID_COUNT[0]*i,0)
        end_pos = (SCREEN_SIZE[0]/GRID_COUNT[0]*i,SCREEN_SIZE[1])
        pygame.draw.line(screen, BLACK, start_pos, end_pos, 1)
    for i in range(GRID_COUNT[1]):
        start_pos = (0,SCREEN_SIZE[1] / GRID_COUNT[1] * i)
        end_pos = (SCREEN_SIZE[0],SCREEN_SIZE[1] / GRID_COUNT[1] * i)
        pygame.draw.line(screen, BLACK, start_pos, end_pos, 1)



GridMap = [[cell(y,x) for x in range(HORIZ_COUNT)] for y in range(VERTIC_COUNT)]

screen = pygame.display.set_mode(SCREEN_SIZE)

clock = pygame.time.Clock()

target_position = np.array([np.random.randint(0,HORIZ_COUNT),np.random.randint(0,VERTIC_COUNT)])


agent1 = agent()

done = False
last_action = None
last_position = None
pygame.time.set_timer(MOVEEVENT, MOVEEVENT_T)

correct = 1
udp = Udp()
while done == False:

    screen.fill(WHITE)
    colorGrid()
    drawGrid()
    udp.update()
    drawTarget(target_position)
    agent1.draw_agent()
    if correct!= -1 :
        correct = 1

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN:
            pressedKey = event.key
            if(pressedKey == pygame.K_w):
                correct = -1

        if event.type == pygame.USEREVENT:
            if event.data == 30:
                updatePropabilities(last_position, last_action, correct)
                last_action, last_position = agent1.next_step()
                if np.all(agent1.position == target_position):
                    agent1 = agent()
                updateLikelihood()
                correct = 1
            else:
                print("error: "+str(event.data))



    pygame.display.flip()



