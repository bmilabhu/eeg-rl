
%% Initiate the triger sending

addpath ./io64

global cogent;

%connect to port
config_io;

% optional step: verify that the inpoutx64 driver was successfully initialized
if( cogent.io.status ~= 0 )
    error('inp/outp installation failed');
end


% LPT port address
address =  53504; %Device manager -> Ports -> Resources
% address = 53264;

%% Sending the triger

v = [1:20];
% v = kron(v, [1 1 1])
for i = 1:length(v)
    v(i)
    outp(address,0);
    %sending
    outp(address,v(i));
    pause(1/256);    
    outp(address,0);
    pause(1)
end
%     outp(address,0);