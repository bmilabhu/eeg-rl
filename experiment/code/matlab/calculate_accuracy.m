% if featureInfo or sequenceData are not in workspace, they can be found in:
%    C:\Users\sareld\Documents\eeg-rl\eeg-rl\analysis_files\ [SD_04-Apr-2018-train1]
% sequenceData:is a vector in which for every action done in the experiment
% or training, it has 1 if the step was for the right direction and 0 if it
% was for the wrong direction (after the train we take this parameter to be
% sequenceData.seq1)
% AUCperTrial is taken from featureInfo.AUCperTrial it describes the area
% under curve for each trial

function calculate_accuracy(AUCperTrial,sequenceData)

% choose threshcold
min_auc=min(AUCperTrial);
max_auc=max(AUCperTrial);
theta = linspace(min_auc,max_auc,100);
hits_per_theta=[];
true_negatives_per_theta=[];
num_of_wrong_steps=sum(not(sequenceData));
num_of_right_steps=sum(sequenceData);
for t=theta
   threshold_auc=AUCperTrial;
   threshold_auc(AUCperTrial>t)=1;
   threshold_auc(AUCperTrial<=t)=0;
   
   
   
   %the precentage of wrong steps that got a surprise signal out of all wrong
   %steps
   hits = (sum(threshold_auc>sequenceData)/num_of_wrong_steps);
   hits_per_theta=[hits_per_theta [hits]];
   %the precentage of right steps that didn't get surprise signal out of
   %all right steps
   true_negatives=(sum(threshold_auc<sequenceData)/num_of_right_steps);
   true_negatives_per_theta=[true_negatives_per_theta [true_negatives]]; 
end
figure('position',[488         402        1422         360])
subplot(1,3,1)
plot(theta,hits_per_theta,'LineWidth',3)
title('Hits per theta')
ylabel('Hits')
xlabel('Theta')

subplot(1,3,2)
plot(theta,1-true_negatives_per_theta,'LineWidth',3)
title('false positive per theta')
ylabel('false positive')
xlabel('Theta')

subplot(1,3,3)
plot(1-true_negatives_per_theta,hits_per_theta,'LineWidth',3)
hold on
linear_line=0:0.1:1;
plot(linear_line,linear_line,'r','LineWidth',2);
hold off
legend('hits per false positive','linear line');
title('hits per false positive')
xlabel('False positives')
ylabel('Hits')
chosen_theta=36.74;

% calculate accuracy
% note that:
% AUCperTrial(i) = 1 -> surprise trial 
% AUCperTrial(i) = 0 -> no surprise trial 
% seq1(i) = 1 -> no error trial 
% seq1(i) = 0 -> error trial 
chosen_theta_auc=AUCperTrial;
chosen_theta_auc(AUCperTrial>chosen_theta)=1;
chosen_theta_auc(AUCperTrial<=chosen_theta)=0;
falsePositives = sum(chosen_theta_auc==1 & sequenceData==1)/sum(sequenceData==1)
hits = sum(chosen_theta_auc==1 & sequenceData==0)/sum(sequenceData==0)
accuracy=sum(xor(chosen_theta_auc,sequenceData))/(num_of_wrong_steps+num_of_right_steps)




