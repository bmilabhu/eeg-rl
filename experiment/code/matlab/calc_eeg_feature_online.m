function feature = calc_eeg_feature_online(eegData,featureInfo)
[nCh nSamplesPerTrial nTrials] = size(eegData);

withEOG = (nCh == 14);
   
eegDataRR = reref(eegData, [9,10], 'exclude',[11:14]);
nCh = size(eegDataRR,1);

if ~withEOG 
    options ={[1:8],   [0.1000],    [30],    [2],    [0] ,   [1],    'boundary'};
else 
    options ={[1:12],   [0.1000],    [30],    [2],    [0] ,   [1],    'boundary'};
end
% options = {[1:8] , 'Cutoff', [ 0.1 30], 'Design', 'butter', 'Filter', 'bandpass', 'Order',  2 };
EEGOnline.data = eegDataRR;
EEGOnline.srate = 256;
EEGOnline.pnts = nSamplesPerTrial;
EEGOnline.epoch = [];
EEGOnline.nbchan = nCh;
EEGOnline.trials = 1;
EEGOnline.event = [];
% [EEGOnline, ferror] = basicfilter( EEGOnline, options{:});
EEGOnline = pop_eegfiltnew(EEGOnline,0.5,30,846,0,[],0);
% [EEGOnline, ferror]   = pop_basicfilter( EEGOnline,  1:8 , 'Cutoff', [ 0.1 30], 'Design', 'butter', 'Filter', 'bandpass', 'Order',  2 ); % GUI: 06-Dec-2015 09:38:33

% NOTE: basicfilter IS AN EEGLAB FUNCTION BUT APPARENTLY IT DOESN'T APPEAR
% IN ALL VERSIONS SO I COPIED IT TO THE NF SOFTWARE DIRECTORY

icaact = reshape(featureInfo.icaweights*featureInfo.icasphere*reshape(EEGOnline.data(1:size(featureInfo.icaweights,1),:,:),[size(featureInfo.icaweights,1) nSamplesPerTrial*nTrials]),[size(featureInfo.icaweights,1) nSamplesPerTrial nTrials]);
compData = icaact;
% Wfilter = featureInfo.Wfilter;
% compData = Wfilter*eegData; %eegdata - 1.2 secs of data

if strcmpi(featureInfo.type,'auc')    
    ind_t1 = featureInfo.ind_t1; % -51;
    ind_t2 = featureInfo.ind_t2; % -20;
    ch = featureInfo.selectedCh;   
    auc = sum(compData(ch,ind_t1:ind_t2),2);
    feature = auc;
end


