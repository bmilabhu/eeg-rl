
function varargout = eeg_rl_online(varargin)
% EEG_RL_ONLINE MATLAB code for eeg_rl_online.fig
%      EEG_RL_ONLINE, by itself, creates a new EEG_RL_ONLINE or raises the existing
%      singleton*.
%
%      H = EEG_RL_ONLINE returns the handle to a new EEG_RL_ONLINE or the handle to
%      the existing singleton*.
%
%      EEG_RL_ONLINE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EEG_RL_ONLINE.M with the given input arguments.
%
%      EEG_RL_ONLINE('Property','Value',...) creates a new EEG_RL_ONLINE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before eeg_rl_online_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to eeg_rl_online_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help eeg_rl_online

% Last Modified by GUIDE v2.5 18-Dec-2017 19:49:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @eeg_rl_online_OpeningFcn, ...
                   'gui_OutputFcn',  @eeg_rl_online_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function handles_out =LoadModel(handles)
global params;
modelName=params.modelName;


if isempty(find_system('Type','block_diagram','Name',modelName))
    load_system(modelName);
end

% Store model's parameters, in order to restore them in the end
params.OriginalStopTime = get_param(modelName,'Stoptime');
params.OriginalMode = get_param(modelName,'SimulationMode');
params.OriginalStartFcn = get_param(modelName,'StartFcn');

% Add a listener to the 'Scope' block

% Add a listener to the 'Scope' block


Scope = struct(...
    'blockName','',...
    'blockHandle',[],...
    'blockEvent','',...
    'blockFcn',[]);

Scope.blockName = sprintf('%s/Scope',modelName);
Scope.blockHandle = get_param(Scope.blockName,'Handle');
Scope.blockEvent = 'PostOutputs';
Scope.blockFcn = @ScopeCallback;
params.Scope=Scope;
handles_out=handles;


% --- Executes just before eeg_rl_online is made visible.
function eeg_rl_online_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to eeg_rl_online (see VARARGIN)
global params;

u = udp('10.0.0.1' ,27643); 
%u = udp('127.0.0.1' ,27643); 
u.OutputBufferSize = 4;
fopen(u);

params.modelName = 'exp_ERP_NF_huji';
%params.modelName = 'EEG2MATnoEEG';
handles=LoadModel(handles);
guidata(hObject, handles);

global timer
timer = 0;
params.u = u;
params.start = 10000;
params.stop = 20000;
params.move_agent = 30000;
params.amplitude_mat=[];

% Choose default command line output for eeg_rl_online
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes eeg_rl_online wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = eeg_rl_online_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
global params;
varargout{1} = handles.output;



% --- Executes on button press in start.
function start_Callback(hObject, eventdata, handles)
% hObject    handle to start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global params;
Scope=params.Scope;
global eventHandle
u = params.u;

analysisFilesDir = '..\..\..\analysis_files'
files = dir (analysisFilesDir);
dirFlags = [files.isdir];
datasetList = {files(dirFlags).name};

[datasetInd,ok] = listdlg('PromptString','Select a dataset:',...
    'SelectionMode','single',...
    'ListSize',[300, 500],...
    'ListString',datasetList);
 

featureInfoFile = [analysisFilesDir,'\',datasetList{datasetInd},'\','featureInfo.mat'];
sequenceDataFile =  [analysisFilesDir,'\',datasetList{datasetInd},'\','sequenceData.mat'];
f = load(featureInfoFile);
s = load(sequenceDataFile);

params.featureInfo = f.featureInfo;
params.sequenceData = s.sequenceData;


calculate_accuracy(params.featureInfo.AUCperTrial,params.sequenceData.seq1)
fwrite(params.u,params.start,'float');

% set the stop time to Session_Time
set_param(params.modelName,'StopTime','inf');
%set_param(modelName,'StopTime','inf');

% set the simulation mode to normal
set_param(params.modelName,'SimulationMode','normal');



% start the model
set_param(params.modelName,'SimulationCommand','start');

% Set a listener
eventHandle = add_exec_event_listener(Scope.blockName,Scope.blockEvent, Scope.blockFcn);




% --- Executes on button press in pushbutton5.
function stop_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global params;
u = params.u;

d = createMessage(params.stop,1);
fwrite(u,params.stop);
% start the model
set_param(params.modelName,'SimulationCommand','stop');
%fclose(u);

function ScopeCallback(block, eventdata) %#ok

global params;
global timer;
timer = timer+1;
params.amplitude_mat(:,timer)=[block.InputPort(1).Data(1) ; block.InputPort(1).Data(2); block.InputPort(1).Data(3); block.InputPort(1).Data(4); block.InputPort(1).Data(5); block.InputPort(1).Data(6); block.InputPort(1).Data(7); block.InputPort(1).Data(8); block.InputPort(1).Data(9);block.InputPort(1).Data(10)];
if(timer==256)
    disp(0);
    %fwrite(params.u, 1:250, 'int32');
    fwrite(params.u,params.move_agent,'float');
    timer=0;
end
if(timer==217)
    disp(1337);
    cur_feature = calc_eeg_feature_online(params.amplitude_mat,params.featureInfo);
    %cur_feature = (cur_feature-params.featureInfo.meanRegressParams.b(1))/params.featureInfo.meanRegressParams.b(2);
    %cur_feature = (cur_feature)/1000;
    disp('cur_feature: ')
    disp(cur_feature)
    disp(createMessage(cur_feature))
    fwrite(params.u,cur_feature,'float');
    params.amplitude_mat=[];
end




