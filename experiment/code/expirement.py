import pygame
import numpy as np
import socket
import struct

##from psychopy import core
##from psychopy import parallel
use_udp=False
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

HORIZ_COUNT = 6
VERTIC_COUNT = 6
LEARNING_RATE = 1000
SCREEN_SIZE = np.array([800, 600])
GRID_COUNT = np.array([HORIZ_COUNT, VERTIC_COUNT])
RECT_SIZE = SCREEN_SIZE / GRID_COUNT

MOVEEVENT = pygame.USEREVENT
MOVEEVENT_T = 1000

threshold = 0

KEY_UP = "up"
KEY_DOWN = "down"
KEY_RIGHT = "right"
KEY_LEFT = "left"

ACTIONS = {KEY_UP: np.array([0, -1]),
           KEY_DOWN: np.array([0, 1]),
           KEY_RIGHT: np.array([1, 0]),
           KEY_LEFT: np.array([-1, 0]), }


class QLearningTable:
    def __init__(self, target_position, learning_rate=1, reward_decay=1, e_greedy=1):
        self.GridMap = [[cell(y, x) for x in range(VERTIC_COUNT)] for y in range(HORIZ_COUNT)]
        self.actions = list(ACTIONS.keys())  # a list
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        # self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.target_position = target_position

    def learn(self, s, a, r, s_):
        # self.check_state_exist(s_)
        # q_predict = self.q_table.ix[str(s), a]
        if (a != None):
            if r == 1:
                print("reached!")
            elif r == -1:
                print("incorrect!")
        q_predict = self.GridMap[s[0]][s[1]].action_reward[a]

        if (s_ == self.target_position).all():  # next state is not terminal

            q_target = r
        else:  # next state is terminal
            q_target = r + self.gamma * max(self.GridMap[s_[0]][s_[1]].action_reward.values())
        self.GridMap[s[0]][s[1]].action_reward[a] += self.lr * (q_target - q_predict)  # update
        # print('...........................................')
        # for i in range(HORIZ_COUNT):
        #     for j in range(VERTIC_COUNT):
        #         print('.............')
        #         #print(str(i) + "," + str(j) + ": ", end="")
        #         print(RL.GridMap[i][j].action_reward)
        # print('...........................................')

    def getValueScale(self):
        maxV = self.GridMap[0][0].getValue()
        minV = self.GridMap[0][0].getValue()
        for i in range(HORIZ_COUNT):
            for j in range(VERTIC_COUNT):
                if self.GridMap[i][j].getValue() > maxV:
                    maxV = self.GridMap[i][j].getValue()
                if self.GridMap[i][j].getValue() < minV:
                    minV = self.GridMap[i][j].getValue()
        return maxV, minV

class Udp():

    def __init__(self):
        UDP_IP="127.0.0.1"
        UDP_PORT=27643
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setblocking(0)
        self.sock.bind((UDP_IP,UDP_PORT))

    def update(self):
        try:
            data, addr = self.sock.recvfrom(4)
            # data=data.decode('ascii')
            data = struct.unpack(">f", data)[0]
            print(data)
            # data = int("{:02}".format(ord(data)))
            ev = pygame.event.Event(pygame.USEREVENT, {'data': data, 'addr': addr})
            # print(data)
            pygame.event.post(ev)
        except socket.error as e:
            pass

class cell:
    def __init__(self, x, y):
        self.position = np.array([x, y])
        self.action_reward = {}
        self.likelihood = 0
        if (x == 0 and y == 0):
            self.likelihood = 1

        for actionName in ACTIONS:
            self.action_reward[actionName] = 0
        if (x == HORIZ_COUNT - 1):
            self.action_reward.pop(KEY_RIGHT, None)
        if (x == 0):
            self.action_reward.pop(KEY_LEFT, None)
        if (y == 0):
            self.action_reward.pop(KEY_UP, None)
        if (y == VERTIC_COUNT - 1):
            self.action_reward.pop(KEY_DOWN, None)
            # self.normalizeProbs()

    # def updateProb(self, action, p):
    #     self.action_reward[action] = p
    #     # self.normalizeProbs()

    def normalizeProbs(self):
        sum = 0
        for actionName in self.action_reward:
            sum = sum + self.action_reward[actionName]
        for actionName in self.action_reward:
            self.action_reward[actionName] = self.action_reward[actionName] / float(sum)

    def getValue(self):
        return max(self.action_reward.values())

    def get_probabilities(self):
        action_reward = self.action_reward
        maxV = max(action_reward.values())
        minV = min(action_reward.values())
        probs = {}
        for k, v in action_reward.items():
            if maxV - minV != 0:
                probs[k] = 2**(1000*(v - minV) / float(maxV - minV))-1
            else:
                probs[k] = 1
        s = sum(probs.values())
        for k, v in probs.items():
            probs[k] /= float(s)
        return probs


class agent():
    def __init__(self):
        # self.position = np.array([HORIZ_COUNT/2,VERTIC_COUNT/2]).astype(np.int)
        self.position = np.array([0, 0]).astype(np.int)

    def draw_agent(self):
        x = RECT_SIZE[0] * self.position[0]
        y = RECT_SIZE[1] * self.position[1]
        rect = [x, y, RECT_SIZE[0], RECT_SIZE[1]]
        pygame.draw.rect(screen, BLUE, rect, 0)

    def next_step(self):
        action_reward = RL.GridMap[self.position[0]][self.position[1]].action_reward

        probs = np.array(list(RL.GridMap[self.position[0]][self.position[1]].get_probabilities().values()))

        action = np.random.choice(list(action_reward.keys()), p=probs)
        # if np.random.uniform() < RL.epsilon:
        #     # choose best action
        #     items = list(action_reward.items())
        #     random.shuffle(items)
        #     items = OrderedDict(items)
        #     action = max(items, key=items.get)
        # else:
        #     # choose random action
        #     action = np.random.choice(list(action_reward.keys()))
        last_position = self.position
        self.position = self.position + ACTIONS[action]
        return action, last_position


def updateLikelihood():
    l1 = l2 = l3 = l4 = 0
    for i in range(HORIZ_COUNT):
        for j in range(VERTIC_COUNT):

            try:
                l1 = RL.GridMap[i - 1][j].likelihood * RL.GridMap[i - 1][j].get_probabilities()[KEY_RIGHT]
            except:
                pass
            try:
                l2 = RL.GridMap[i + 1][j].likelihood * RL.GridMap[i + 1][j].get_probabilities()[KEY_LEFT]
            except:
                pass
            try:
                l3 = RL.GridMap[i][j - 1].likelihood * RL.GridMap[i][j - 1].get_probabilities()[KEY_DOWN]
            except:
                pass
            try:
                l4 = RL.GridMap[i][j + 1].likelihood * RL.GridMap[i][j + 1].get_probabilities()[KEY_UP]
            except:
                pass
            RL.GridMap[i][j].likelihood = max(l1, l2, l3, l4)  # ,RL.GridMap[i][j].likelihood)
            if (i == 0 and j == 0):
                RL.GridMap[i][j].likelihood = 1


def colorGrid():
    for i in range(HORIZ_COUNT):
        for j in range(VERTIC_COUNT):
            x = RECT_SIZE[0] * i
            y = RECT_SIZE[1] * j
            cell = RL.GridMap[i][j]
            rect = [x, y, RECT_SIZE[0], RECT_SIZE[1]]

            pygame.draw.rect(screen, (int(min(255, (cell.likelihood) * 255)), 255, 255), rect, 0)


# def updatePropabilities(position, action, correct):
#     if (action != None and correct != 0):
#         if correct == 1:
#             print("correct!")
#             m = LEARNING_RATE
#         elif correct == -1:
#             print("incorrect!")
#             m = 1 / LEARNING_RATE
#         x = (RL.GridMap[position[0]][position[1]].action_reward[action] * m)
#         # x += max(RL.GridMap[position[0]+ACTIONS[action][0]][position[1]+ACTIONS[action][1]].actions_probs.values())
#         p = x * m / (1 + (m - 1) * x)
#         RL.GridMap[position[0]][position[1]].updateProb(action, p)


def drawTarget(position):
    x = RECT_SIZE[0] * position[0]
    y = RECT_SIZE[1] * position[1]
    rect = [x, y, RECT_SIZE[0], RECT_SIZE[1]]
    pygame.draw.rect(screen, RED, rect, 0)


def drawGrid():
    for i in range(GRID_COUNT[0]):
        start_pos = (SCREEN_SIZE[0] / GRID_COUNT[0] * i, 0)
        end_pos = (SCREEN_SIZE[0] / GRID_COUNT[0] * i, SCREEN_SIZE[1])
        pygame.draw.line(screen, BLACK, start_pos, end_pos, 1)
    for i in range(GRID_COUNT[1]):
        start_pos = (0, SCREEN_SIZE[1] / GRID_COUNT[1] * i)
        end_pos = (SCREEN_SIZE[0], SCREEN_SIZE[1] / GRID_COUNT[1] * i)
        pygame.draw.line(screen, BLACK, start_pos, end_pos, 1)


def draw_screen():
    screen.fill(WHITE)
    colorGrid()
    drawGrid()
    drawTarget(target_position)
    agent1.draw_agent()


if __name__ == '__main__':

    screen = pygame.display.set_mode(SCREEN_SIZE)

    clock = pygame.time.Clock()

    target_position = np.array([np.random.randint(0, HORIZ_COUNT), np.random.randint(0, VERTIC_COUNT)])
    #target_position = np.array([0, 3])
    agent1 = agent()

    done = False
    RL = QLearningTable(target_position)
    cur_position = agent1.position
    last_position = np.array([])
    cur_action = np.array([])

    draw_screen()
    pygame.display.flip()
    pygame.time.wait(2000)
    if not use_udp:
        pygame.time.set_timer(MOVEEVENT, MOVEEVENT_T)
    reward = 0
    udp = Udp()
    while done == False:

        # if reward != -1:#todo ask why sarel did this
        #     # print('reward: '+str(reward))
        #     reward = 1
        if use_udp:
            udp.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
            if event.type == pygame.KEYDOWN:
                pressedKey = event.key
                if (pressedKey == pygame.K_w):
                    reward = -1

            if event.type == pygame.USEREVENT:
                if use_udp:
                    event_data = event.data
                else:
                    event_data = 30

                if event_data == 30:
                    # updatePropabilities(cur_position, cur_action, reward)
                    if (last_position.size != 0 and cur_position.size != 0):
                        RL.learn(last_position, cur_action, reward, cur_position)
                        reward = 0

                    cur_action, last_position = agent1.next_step()
                    cur_position = agent1.position
                    if np.all(agent1.position == target_position):
                        reward = 10
                    if np.all(last_position ==target_position):
                        agent1 = agent()

                    updateLikelihood()
                else:
                    print("error: = ", event_data)
                    if event_data>threshold:
                        reward = -1



        draw_screen()
        pygame.display.flip()
