import pygame
import numpy as np
import random
import datetime
import time
from psychopy import core
from psychopy import parallel
triggerLen=1.0/256
portNum=53504

port=parallel.ParallelPort(portNum)
port.setData(0)
iterations = 4
loopTimes = 60
highLightIterations = 4
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BLUE = (255, 0, 0)
GREEN = (0, 255, 0)
RED = (255, 255, 255)


TRIGG_START_BLOCK = 100
NUM_OF_DIRECTIONS=4
TRIGG_MIDDEL = 10
TRIGG_UP = 20
TRIGG_LEFT = 30
TRIGG_DOWN = 40
TRIGG_RIGHT = 50
WAIT_TIME = 0.5

TRIGGERS = [TRIGG_RIGHT,TRIGG_DOWN,TRIGG_LEFT,TRIGG_UP]


TRIGGER_FILE = "T:\\eeg-rl\\data\\TrigFile"
st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H-%M-%S')

trigFile =open(TRIGGER_FILE+"_"+st+".txt",'w')

size = np.array([1920, 1080])
middle = size / 2
ballSize = 30
distance = ballSize + ballSize * 5

screen = pygame.display.set_mode(size,pygame.FULLSCREEN)
locations = np.array([middle + distance * np.array([1, 0]),
                      #middle + distance * np.array([1, 1]),
                      middle + distance * np.array([0, 1]),
                      #middle + distance * np.array([-1, 1]),
                      middle + distance * np.array([-1, 0]),
                      #middle + distance * np.array([-1, -1]),
                      middle + distance * np.array([0, -1])])
                      #middle + distance * np.array([1, -1])])
iteration_probabilities_of_chosen_dot=np.array([0.95,0.85,0.75,0.55])
iteration_probabilities_of_non_chosen_dot=(1- iteration_probabilities_of_chosen_dot)/(NUM_OF_DIRECTIONS-1)



def drawDots():
    for location in locations:
        pygame.draw.circle(screen, RED, location, 2)



def highlightDot(location):
    location = locations[dotChosen]
    for i in range(highLightIterations):
        pygame.event.get()
        pygame.draw.circle(screen, BLACK, location, 2)
        pygame.display.update()
        core.wait(0.5)
        #time.sleep(0.5)
        pygame.draw.circle(screen, RED, location, 2)
        pygame.display.update()
        core.wait(0.5)
        #time.sleep(0.5)

def sendTrigger(trigNum):
    print("Trig: "+ str(trigNum))
    trigFile.write(str(trigNum) + "\n")
    port.setData(trigNum)
    core.wait(triggerLen)
    #time.sleep(triggerLen)
    port.setData(0)

def runIterarion(probabilities_vec):
    for i in range(loopTimes):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                trigFile.close()
                exit()
            if event.type == pygame.KEYDOWN:
                pressedKey = event.key
                if (pressedKey == pygame.K_ESCAPE):
                    trigFile.close()
                    exit()
        randint=np.random.choice(np.arange(0, len(locations)), p=probabilities_vec.reshape(probabilities_vec.size))
        print ("rand: " + str(randint))
        # randint = random.randint(0, len(locations)-1)
        location = locations[randint]
        screen.fill(BLACK)
        drawDots()
        pygame.draw.circle(screen, BLUE, location, ballSize)
        pygame.display.update()
        sendTrigger(TRIGGERS[randint])
        core.wait(WAIT_TIME)

        screen.fill(BLACK)
        drawDots()
        pygame.draw.circle(screen, BLUE, (size[0] / 2, size[1] / 2), ballSize)
        pygame.display.update()
        sendTrigger(TRIGG_MIDDEL)
        core.wait(WAIT_TIME/2.0)


bigBall = pygame.draw.circle(screen, BLUE, (int(size[0] / 2), int(size[1] / 2)), ballSize)
pygame.display.update()
drawDots()
for j in range(iterations):
    pygame.event.get()
    sendTrigger(TRIGG_START_BLOCK)
    dotChosen = random.randint(0, NUM_OF_DIRECTIONS-1)
    print ("chosen: "+str(dotChosen))
    core.wait(0.1)
    sendTrigger(TRIGGERS[dotChosen])
    highlightDot(dotChosen)

    core.wait(0.05)
    probabilities=np.empty((NUM_OF_DIRECTIONS,1))
    probabilities.fill(iteration_probabilities_of_non_chosen_dot[j])
    probabilities[dotChosen]=iteration_probabilities_of_chosen_dot[j]
    runIterarion(probabilities)
