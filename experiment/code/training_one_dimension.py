import pygame
import numpy as np
import socket
import struct
import time
import datetime
import matplotlib.pyplot as plt
from psychopy import core
from psychopy import parallel
import pickle


use_udp = False

#colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
YELLOW = (255,255,0)

#triggers
TRIGG_UP = 20
TRIGG_LEFT = 30
TRIGG_DOWN = 40
TRIGG_RIGHT = 50

#accuracy params
correct_counter = 0
trials_count = 0

#screen params
HORIZ_COUNT = 20
VERTIC_COUNT = 1
LEARNING_RATE = 1000
SCREEN_SIZE = np.array([HORIZ_COUNT * 60, VERTIC_COUNT * 60])
GRID_COUNT = np.array([HORIZ_COUNT, VERTIC_COUNT])
RECT_SIZE = SCREEN_SIZE / GRID_COUNT

#events params
MOVEEVENT = pygame.USEREVENT
MOVEEVENT_T = 1000

#udp commands
START_COMMAND = 10000
STEP_COMMAND = 30000
QUIT_COMMAND = 20000

threshold = 36.74

triggerLen=1.0/256
portNum=53504
port=parallel.ParallelPort(portNum)
# possible actions
KEY_UP = "up"
KEY_DOWN = "down"
KEY_RIGHT = "right"
KEY_LEFT = "left"
TRIGGERS = {KEY_UP: 20,
            KEY_DOWN:40,
            KEY_RIGHT:50,
            KEY_LEFT: 30}

ACTIONS = {KEY_UP: np.array([0, -1]),
           KEY_DOWN: np.array([0, 1]),
           KEY_RIGHT: np.array([1, 0]),
           KEY_LEFT: np.array([-1, 0]), }

STEPS_IN_ITERATION = 60



PROBS_ARRAY = [(0.95,False),(0.95,True),(0.85,False),(0.85,True),(0.75,False),(0.75,True),(0.55,False),(0.55,True),
               (0.35,False),(0.35,True),(0.20,False),(0.20,True),(0.10,False),(0.10,True)]
TRIGG_START_BLOCK = 100

CORRECT_STEP = TRIGGERS[KEY_RIGHT]

TRIGGER_FILE = "T:\\eeg-rl\\analysis_files\\train_Trigs"
SEQUENCE_DATA_FILE = "T:\\eeg-rl\\analysis_files\\train_EEG_featurs"
#TRIGGER_FILE = "Expirement_Trigs"
#SEQUENCE_DATA_FILE = "Expirement_sequenceData"
#TRIGGER_FILE = "T:\\eeg-rl\\analysis_files\\Expirement_Trigs"
#SEQUENCE_DATA_FILE = "T:\\eeg-rl\\analysis_files\\Expirement_sequenceData"
#TRIGGER_FILE = "Expirement_Trigs"
#SEQUENCE_DATA_FILE = "Expirement_sequenceData"

st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H-%M-%S')

trigFile = open(TRIGGER_FILE+"_"+st+".txt",'w')
sequenceData = open(SEQUENCE_DATA_FILE+"_"+st+".txt",'w')

targetInRight = True

class QLearningTable:
    """
    ititialize the q learning table
    """
    def __init__(self, target_position, learning_rate=1, reward_decay=1, e_greedy=1):
        self.GridMap = [[cell(y, x) for x in range(VERTIC_COUNT)] for y in range(HORIZ_COUNT)]
        self.actions = list(ACTIONS.keys())
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.target_position = target_position

    def learn(self, s, a, r, s_):
        # self.check_state_exist(s_)
        # q_predict = self.q_table.ix[str(s), a]
        if (a != None):
            if r == 1:
                print("reached!")
            elif r == -1:
                print("incorrect!")
        q_predict = self.GridMap[s[0]][s[1]].action_reward[a]

        if (s_ == self.target_position).all():  # next state is not terminal

            q_target = r
        else:  # next state is terminal
            q_target = r + self.gamma * max(self.GridMap[s_[0]][s_[1]].action_reward.values())
        self.GridMap[s[0]][s[1]].action_reward[a] += self.lr * (q_target - q_predict)  # update

"""
create a udp socket to connect with the computer with the eeg
"""
class Udp():
    def __init__(self):
        UDP_IP = ""
        UDP_PORT = 27643
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setblocking(0)
        self.sock.bind((UDP_IP, UDP_PORT))

    def update(self):
        try:
            data, addr = self.sock.recvfrom(4)
            data = struct.unpack(">f", data)[0]
            ev = pygame.event.Event(pygame.USEREVENT, {'data': data, 'addr': addr})
            pygame.event.post(ev)
        except socket.error as e:
            pass
"""
represents one cell in the grid that is shown on the screen
"""
class cell:
    def __init__(self, x, y):
        self.position = np.array([x, y])
        self.action_reward = {}
        self.likelihood = 0
        if (x == 0 and y == 0):
            self.likelihood = 1

        for actionName in ACTIONS:
            self.action_reward[actionName] = 0
        if (x == HORIZ_COUNT - 1):
            self.action_reward.pop(KEY_RIGHT, None)
        if (x == 0):
            self.action_reward.pop(KEY_LEFT, None)
        if (y == 0):
            self.action_reward.pop(KEY_UP, None)
        if (y == VERTIC_COUNT - 1):
            self.action_reward.pop(KEY_DOWN, None)

    def get_probabilities(self):
        action_reward = self.action_reward
        maxV = max(action_reward.values())
        minV = min(action_reward.values())
        probs = {}
        for k, v in action_reward.items():
            if maxV - minV != 0:
                probs[k] = 2 ** (1000 * (v - minV) / float(maxV - minV)) - 1
            else:
                probs[k] = 1
        s = sum(probs.values())
        for k, v in probs.items():
            probs[k] /= float(s)
        return probs


stepCount = 0
iteration = -1

"""
represents the agent that is moving on the grid during the training
"""
class agent():
    def __init__(self):
        # self.position = np.array([HORIZ_COUNT/2,VERTIC_COUNT/2]).astype(np.int)
        if targetInRight:
            self.position = np.array([0, 0]).astype(np.int)
        else:
            self.position = np.array([HORIZ_COUNT - 1, 0]).astype(np.int)
        self.probsArray = PROBS_ARRAY
    """
    draws the agent
    """
    def draw_agent(self):
        x = RECT_SIZE[0] * self.position[0]
        y = RECT_SIZE[1] * self.position[1]
        rect = [x, y, RECT_SIZE[0], RECT_SIZE[1]]
        pygame.draw.rect(screen, YELLOW, rect, 0)
    """
    returns the next step of the agent according to predefined probability array, and the last position of the agent
    """
    def next_step(self):
        action_reward = RL.GridMap[self.position[0]][self.position[1]].action_reward
        actions_list = list(action_reward.keys())
        actions_list.sort()
        # probs = np.array(list(RL.GridMap[self.position[0]][self.position[1]].get_probabilities().values()))
        if (len(list(action_reward.keys())) == 2):
            prob = PROBS_ARRAY[iteration][0]
            print(prob)
            print(actions_list)
            if targetInRight:
                probs = np.array([1-prob,prob])
            else:
                probs = np.array([prob, 1 - prob])
        else:
            probs = np.array([1])

        action = np.random.choice(actions_list, p=probs)
        last_position = self.position
        self.position = self.position + ACTIONS[action]
        sendTrigger(TRIGGERS[action])


        return action, last_position

"""
updates the probabilities that will be used to color the grid
"""
def updateLikelihood():
    l1 = l2 = l3 = l4 = 0
    for i in range(HORIZ_COUNT):
        for j in range(VERTIC_COUNT):

            try:
                l1 = RL.GridMap[i - 1][j].likelihood * RL.GridMap[i - 1][j].get_probabilities()[KEY_RIGHT]
            except:
                pass
            try:
                l2 = RL.GridMap[i + 1][j].likelihood * RL.GridMap[i + 1][j].get_probabilities()[KEY_LEFT]
            except:
                pass
            try:
                l3 = RL.GridMap[i][j - 1].likelihood * RL.GridMap[i][j - 1].get_probabilities()[KEY_DOWN]
            except:
                pass
            try:
                l4 = RL.GridMap[i][j + 1].likelihood * RL.GridMap[i][j + 1].get_probabilities()[KEY_UP]
            except:
                pass
            RL.GridMap[i][j].likelihood = max(l1, l2, l3, l4)  # ,RL.GridMap[i][j].likelihood)
            if (i == 0 and j == 0):
                RL.GridMap[i][j].likelihood = 1

"""
colors the grid
"""
def colorGrid():
    for i in range(HORIZ_COUNT):
        for j in range(VERTIC_COUNT):
            x = RECT_SIZE[0] * i
            y = RECT_SIZE[1] * j
            cell = RL.GridMap[i][j]
            rect = [x, y, RECT_SIZE[0], RECT_SIZE[1]]

            #pygame.draw.rect(screen, (int(min(255, (cell.likelihood) * 255)), 255, 255), rect, 0)
            pygame.draw.rect(screen, (0, 0, 0), rect, 0)


"""
draws the target on the grid
"""
def drawTarget(position):
    x = RECT_SIZE[0] * position[0]
    y = RECT_SIZE[1] * position[1]
    rect = [x, y, RECT_SIZE[0], RECT_SIZE[1]]
    pygame.draw.rect(screen, RED, rect, 0)

"""
draws the grid
"""
def drawGrid():
    for i in range(GRID_COUNT[0]):
        start_pos = (SCREEN_SIZE[0] / GRID_COUNT[0] * i, 0)
        end_pos = (SCREEN_SIZE[0] / GRID_COUNT[0] * i, SCREEN_SIZE[1])
        pygame.draw.line(screen, WHITE, start_pos, end_pos, 1)
    for i in range(GRID_COUNT[1]+1):
        start_pos = (0, SCREEN_SIZE[1] / GRID_COUNT[1] * i)
        end_pos = (SCREEN_SIZE[0], SCREEN_SIZE[1] / GRID_COUNT[1] * i)
        pygame.draw.line(screen, WHITE, start_pos, end_pos, 1)

"""
draws the greed. the target, and the agent
"""
def draw_screen():
    screen.fill(WHITE)
    colorGrid()
    drawGrid()
    drawTarget(target_position)
    agent1.draw_agent()

"""
sends to the other computer trigger number that represents the step that was taken by the agent (in our case left or right)
"""
def sendTrigger(trigNum):
    print("Trig: "+ str(trigNum))
    trigFile.write(str(trigNum) + "\n")
    port.setData(trigNum)
    core.wait(triggerLen)
    port.setData(0)

"""
sends to the other computer trigger number that indicates that a new session of the training started, the trigger also 
indicates if the target location
"""
def start_block():
    sendTrigger(TRIGG_START_BLOCK)
    core.wait(triggerLen*5)
    if targetInRight:
        sendTrigger(TRIGGERS[KEY_RIGHT])
    else:
        sendTrigger(TRIGGERS[KEY_LEFT])

right_error_vals = []
left_error_vals = []
if __name__ == '__main__':

    screen = pygame.display.set_mode(SCREEN_SIZE, pygame.FULLSCREEN)

    clock = pygame.time.Clock()
    targetInRight = PROBS_ARRAY[0][1]
    # target_position = np.array([np.random.randint(0, HORIZ_COUNT), np.random.randint(0, VERTIC_COUNT)])
    target_position = np.array([HORIZ_COUNT - 1, 0])
    agent1 = agent()

    done = False
    RL = QLearningTable(target_position)
    cur_position = agent1.position
    last_position = np.array([])
    cur_action = np.array([])


    pygame.display.flip()
    #pygame.time.wait(2000)
    if not use_udp:
        pygame.time.set_timer(MOVEEVENT, MOVEEVENT_T)
    reward = 0
    udp = Udp()

    while done == False:

        if targetInRight:
            target_position = np.array([HORIZ_COUNT - 1, 0])
        else:
            target_position = np.array([0, 0])

        if use_udp:
            udp.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
            if event.type == pygame.KEYDOWN:
                pressedKey = event.key
                if (pressedKey == pygame.K_w):
                    reward = -1
                if (pressedKey == pygame.K_ESCAPE):
                    done = True
            if event.type == pygame.USEREVENT:
                if use_udp:
                    event_data = event.data
                else:
                    event_data = STEP_COMMAND

                if event_data == QUIT_COMMAND:
                    done = True
                if event_data == STEP_COMMAND:

                    # updatePropabilities(cur_position, cur_action, reward)
                    if (last_position.size != 0 and cur_position.size != 0):
                        RL.learn(last_position, cur_action, reward, cur_position)
                        reward = 0

                    if (stepCount % STEPS_IN_ITERATION == 0):
                        iteration += 1
                        if(iteration >= len(PROBS_ARRAY)):
                            done = True
                            break
                        print("iteretion number " + str(iteration))
                        start_block()
                    targetInRight = PROBS_ARRAY[iteration][1]
                    stepCount +=1
                    print(stepCount)
                    cur_action, last_position = agent1.next_step()

                    if (cur_action == KEY_LEFT):
                        print("left")

                    cur_position = agent1.position
                    if np.all(agent1.position == target_position):
                        reward = 0
                    if np.all(last_position == target_position):
                        agent1 = agent()

                    updateLikelihood()
                    print('---------')

                else:
                    if(event_data != STEP_COMMAND and event_data != QUIT_COMMAND and event_data != START_COMMAND ):
                        print("error: = ", event_data)
                        sequenceData.write(str(event_data) + "\n")
                        if event_data > threshold:
                            reward = -1

                        if cur_action == KEY_LEFT:
                            left_error_vals.append(event_data)
                            if event_data > threshold:
                                correct_counter += 1
                        else:
                            right_error_vals.append(event_data)
                            if event_data <= threshold:
                                correct_counter += 1
                        trials_count += 1

        draw_screen()
        pygame.display.flip()
    trigFile.close()
    sequenceData.close()
    left_array = np.array(left_error_vals)
    right_array = np.array(right_error_vals)

    left_avg = np.average(left_array)
    right_avg = np.average(right_array)
    with open('left_right_array_'+st+'.txt', 'wb') as f:
        pickle.dump((left_array,right_array,correct_counter,trials_count),f)

    if(use_udp):
        plt.hist(left_array, bins=10, alpha = 0.5)
        plt.hist(right_array, bins=10, alpha = 0.5)
        plt.title("left avg: "+str(left_avg)+"\nright avg: "+str(right_avg))
        plt.show()
        print ("accuracy = " + str(float(correct_counter) / float(trials_count)))
