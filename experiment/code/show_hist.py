import pickle
import matplotlib.pyplot as plt
import sys
import numpy as np

filename = sys.argv[1]
with open(filename, 'rb') as f:
    (left_array, right_array, correct_counter, trials_count) =pickle.load(f)

left_array[left_array == 20000] = 0
right_array[right_array == 20000] = 0
left_avg = np.average(left_array)
right_avg = np.average(right_array)
plt.hist(left_array, bins=20, alpha = 0.5,label = 'left')
plt.hist(right_array, bins=20, alpha = 0.5, label = 'right')
plt.legend(loc='upper left')
plt.title( "left avg: " +str(left_avg ) + "\nright avg: " +str(right_avg))
plt.show()
print ("accuracy = " + str(float(correct_counter) / float(trials_count)))
