BLOCKS_NUM=4;
ELECTRODES_NUM=8;
MIDDLE_TRIG=10;
UP_TRIG=20;
LEFT_TRIG=30;
DOWN_TRIG=40;
RIGHT_TRIG=50;
BEGINNING_TRIG=514;
BLOCK_START_TRIG=100;
LEFT_EAR=63;
RIGHT_EAR=64;

ON_TARGET_TIME= 1;
MIDDLE_TIME = 0.5;


SAMPLE_RATE = floor(full_data.TimeInfo.Length/full_data.TimeInfo.End);
ON_DOT_SAMPLES = ON_TARGET_TIME*SAMPLE_RATE;
MIDDEL_SAMLPES = MIDDLE_TIME*SAMPLE_RATE;
AFTER_BLOCK_START_SAMPLES = 0.1*SAMPLE_RATE;

%initialization and normalization

data=full_data.data;
time = full_data.time;
[trig,c,trig_v]=find(data(:,end));
trig = cat(2, trig, trig_v);
f = find(conv(trig(:,1),[1,-1],'valid')<AFTER_BLOCK_START_SAMPLES);
trig = removerows(trig,'ind',f+1);
trig = [trig ; size(time,1),BLOCK_START_TRIG];

data=data(:,1:end-1);
electrodes =[12,30,46,48,50,53,57,59];
data_electrodes=data(:,electrodes);
reference=(data(:,LEFT_EAR)+data(:,RIGHT_EAR))/2;
reference = repmat(reference,1,ELECTRODES_NUM);
data_electrodes=data_electrodes-reference;

%partition to blocks 
block_beginnings=trig(trig(:,2)==BLOCK_START_TRIG,1);
data_blocks=cell(BLOCKS_NUM,1);
trig_blocks=cell(BLOCKS_NUM,1);
chosen_dots=cell(BLOCKS_NUM,1);
block_start=cell(BLOCKS_NUM,1);
for i=1:BLOCKS_NUM
    b_start=block_beginnings(i,:);
    b_end=block_beginnings(i+1,:);
    data_blocks{i}=data_electrodes(b_start:b_end,:);
    trig_blocks{i}=trig(find(trig(:,1) == b_start): find(trig(:,1)==b_end),:);
    chosen_dots{i}=trig_blocks{i}(2,2);
    block_start{i}=b_start;
end
changed_triggers=zeros(size(data,1),1);
%find average in every block
averages_of_chosen_dots=cell(BLOCKS_NUM,1);
averages_of_non_chosen_dots=cell(BLOCKS_NUM,1);
for j=1:BLOCKS_NUM
    chosen_dot=chosen_dots{j};
    
    %indices of all chosen dot trigers in trig_block array.
    chosen_dot_i = find(trig_blocks{j}(:,2)==chosen_dot);
    %change triggers numbers
    chosen_dot_i_indices = trig_blocks{j}(chosen_dot_i,1);
    changed_triggers(chosen_dot_i_indices)=1;
    %remove the dot indicator from the beginig of the block
    chosen_dot_i = chosen_dot_i(2:end); 
    
    %take the time marks from the trig_block array.
    start_indices_of_chosen_dot = trig_blocks{j}(chosen_dot_i)-block_start{j};
    end_indices_of_chosen_dot = trig_blocks{j}(chosen_dot_i+1)-block_start{j};
    
    %same for non chosen.
    non_chosen_dot_i = find(trig_blocks{j}(:,2)~=chosen_dot&...
                        trig_blocks{j}(:,2)~=BLOCK_START_TRIG&trig_blocks{j}(:,2)~=BEGINNING_TRIG&...
                         trig_blocks{j}(:,2)~=MIDDLE_TRIG);
    %change triggers numbers
    non_chosen_dot_i_indices = trig_blocks{j}(non_chosen_dot_i,1);
    changed_triggers(non_chosen_dot_i_indices)=2;
    
    %none_chosen_dot_i = datasample(none_chosen_dot_i,size(chosen_dot_i,1));
    
    start_indexes_of_non_chosen_dot = trig_blocks{j}(non_chosen_dot_i)-block_start{j};
    end_indexes_of_non_chosen_dot = trig_blocks{j}(non_chosen_dot_i+1)-block_start{j};
    
    
    %calculate the average of all chosen dot parts
    %start_indices are relative to the beginig of all data, so we make them
    %relative to every block.

    %get the fers dot segment of the block to the average.
    dot_data = data_blocks{j}(start_indices_of_chosen_dot(1):end_indices_of_chosen_dot(1),:);
    if(size(dot_data,1)>=ON_DOT_SAMPLES)
        averages_of_chosen_dots{j} = dot_data(1:ON_DOT_SAMPLES,:);
    end
    for i=2:size(start_indices_of_chosen_dot,1)
          dot_data = data_blocks{j}(start_indices_of_chosen_dot(i):end_indices_of_chosen_dot(i),:);
          if(size(dot_data,1)>=ON_DOT_SAMPLES)
            dot_data = dot_data(1:ON_DOT_SAMPLES,:);
            averages_of_chosen_dots{j} = averages_of_chosen_dots{j} + dot_data;
          end
    end
    
    averages_of_chosen_dots{j} = double(averages_of_chosen_dots{j})/double(size(start_indices_of_chosen_dot,1));
   
    %calculate the average of all non chosen dot parts

    dot_data = data_blocks{j}(start_indexes_of_non_chosen_dot(1):end_indexes_of_non_chosen_dot(1) ,:);
    
    if(size(dot_data,1)>=ON_DOT_SAMPLES)
        averages_of_non_chosen_dots{j} = dot_data(1:ON_DOT_SAMPLES,:);
    end
    
    for i=2:size(start_indexes_of_non_chosen_dot,1)
        dot_data = data_blocks{j}(start_indexes_of_non_chosen_dot(1):end_indexes_of_non_chosen_dot(1),:);
          if(size(dot_data,1)>=ON_DOT_SAMPLES)
            dot_data = dot_data(1:ON_DOT_SAMPLES,:);
            averages_of_non_chosen_dots{j} = averages_of_non_chosen_dots{j} + dot_data;
          end
    end
    
    averages_of_non_chosen_dots{j} = double(averages_of_non_chosen_dots{j})/double(size(start_indexes_of_non_chosen_dot,1));
    
end
%change triggers numbers
data=cat(2,full_data.data,changed_triggers);
average_of_chosen = averages_of_chosen_dots{1};
average_of_non_chosen = averages_of_non_chosen_dots{1};
for i=2:BLOCKS_NUM
    average_of_chosen = average_of_chosen+averages_of_chosen_dots{i};
    average_of_non_chosen = average_of_non_chosen+averages_of_non_chosen_dots{i};
end

%plot
for n=1:8
    subplot(2,4,n);    
    x1 = 1:ON_DOT_SAMPLES;
    x2 = 1:ON_DOT_SAMPLES;
    x1 = x1/SAMPLE_RATE*1000;
    x2 = x2/SAMPLE_RATE*1000;
    hold on
    plot(x1,average_of_chosen(:,n));
    plot(x2,average_of_non_chosen(:,n));
    legend('chosen dot average','non chosen dots average')
    hold off
end


